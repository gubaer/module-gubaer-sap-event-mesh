import ballerina/log;
import ballerina/io;
import gubaer/sap_event_mesh as sem;

const string DEFAULT_CREDENTIALS_FILE_PATH = ".event-mesh-credentials.json";
configurable string credentialsFilePath = DEFAULT_CREDENTIALS_FILE_PATH;


public type Options record {|
    string? queueName = ();
    string? accessType = ();
    string? deadMsgQueue = ();
    int? maxDeliveredUnackedMsgsPerFlow = ();
|};

function buildQueueOptions(Options options) returns json | error {
    var builder = new sem:QueueConfigBuilder();
    if options.accessType !is () {
        builder = builder.withAccessType(<sem:QueueAccessType>options.accessType);
    }
    if options.deadMsgQueue !is () {
        builder = builder.withDeadMsgQueue(<string>options.deadMsgQueue);
    }

    if options.maxDeliveredUnackedMsgsPerFlow !is () {
        builder = builder.withMaxDeliveredUnackedMsgsPerFlow(<int>options.maxDeliveredUnackedMsgsPerFlow);
    }
    return builder.build();
}

function createOrUpdateQueue(Options options) returns error? {
    if options.queueName is () {
        return error("Missing option --queueName. Aborting.");
    }

    var queueOptions = check buildQueueOptions(options);
    string queueName = <string>options.queueName;
    sem:Credentials credentials = check sem:loadCredentials(credentialsFilePath);
    var mgtCredentials = check credentials.credentialsForManagementAPI();

    var tokenEndPoint = check new sem:TokenEndpointClient(mgtCredentials.oa2);
    var token = check tokenEndPoint.requestAccessToken();
    log:printDebug(string`Received access token: ${string:substring(token.access_token, 0, 20)}...`);

    var mgtApiClient = check new sem:ManagementAPIClient(mgtCredentials.uri, accessToken = token.access_token);
    var queue = check mgtApiClient->createOrUpdateQueue(queueName, queueConfig = queueOptions);
    log:printInfo(string`Successfully created queue ${queue.name}`);
}

function getQueue(Options options) returns error? {
    if options.queueName is () {
        return error("Missing option --queueName. Aborting.");
    }

    string queueName = <string>options.queueName;
    sem:Credentials credentials = check sem:loadCredentials(credentialsFilePath);
    var mgtCredentials = check credentials.credentialsForManagementAPI();

    var tokenEndPoint = check new sem:TokenEndpointClient(mgtCredentials.oa2);
    var token = check tokenEndPoint.requestAccessToken();
    log:printDebug(string`Received access token: ${string:substring(token.access_token, 0, 20)}...`);

    var mgtApiClient = check new sem:ManagementAPIClient(mgtCredentials.uri, accessToken = token.access_token);
    var queue = check mgtApiClient->getQueue(queueName);
    log:printInfo(string`Successfully retrieved queue ${queue.name}`);
    io:println(queue.toJsonString());
}

function getQueues(Options options) returns error? {
    sem:Credentials credentials = check sem:loadCredentials(credentialsFilePath);
    var mgtCredentials = check credentials.credentialsForManagementAPI();

    var tokenEndPoint = check new sem:TokenEndpointClient(mgtCredentials.oa2);
    var token = check tokenEndPoint.requestAccessToken();
    log:printDebug(string`Received access token: ${string:substring(token.access_token, 0, 20)}...`);

    var mgtApiClient = check new sem:ManagementAPIClient(mgtCredentials.uri, accessToken = token.access_token);
    var queues = check mgtApiClient->getQueues();
    log:printInfo(string`Successfully retrieved queues`);
    io:println(queues.toJsonString());
}

function deleteQueue(Options options) returns error? {
    if options.queueName is () {
        return error("Missing option --queueName. Aborting.");
    }

    string queueName = <string>options.queueName;
    sem:Credentials credentials = check sem:loadCredentials(credentialsFilePath);
    var mgtCredentials = check credentials.credentialsForManagementAPI();

    var tokenEndPoint = check new sem:TokenEndpointClient(mgtCredentials.oa2);
    var token = check tokenEndPoint.requestAccessToken();
    log:printDebug(string`Received access token: ${string:substring(token.access_token, 0, 20)}...`);

    var mgtApiClient = check new sem:ManagementAPIClient(mgtCredentials.uri, accessToken = token.access_token);
    check mgtApiClient->deleteQueue(queueName);
    log:printInfo(string`Successfully deleted queue ${queueName}`);
}


public function main(string action, *Options options) returns error? {
    match action {
        "create-queue" => {
            check createOrUpdateQueue(options);
        }
        "get-queue" => {
            check getQueue(options);
        }
        "get-queues" => {
            check getQueues(options);
        }
        "delete-queue" => {
            check deleteQueue(options);
        }
        _ => {
            return error("unsupported action", action=action);
        }
    }
}