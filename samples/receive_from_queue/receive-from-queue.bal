import ballerina/log;
import gubaer/sap_event_mesh as sem;
import ballerina/lang.'error;

const string DEFAULT_CREDENTIALS_FILE_PATH = ".event-mesh-credentials.json";
configurable string credentialsFilePath = DEFAULT_CREDENTIALS_FILE_PATH;

public type Options record {|
    string? queueName = ();
    int? qos = ();
|};


public function receiveMessage(Options options) returns error? {
    if options.queueName is () {
        return error("Missing option --queueName. Aborting.");
    }
    sem:QosType qos = sem:DEFAULT_QOS;
    if options.qos != () {
        qos = <sem:QosType> options.qos;
    }

    string queueName = <string>options.queueName;
    sem:Credentials credentials = check sem:loadCredentials(credentialsFilePath);
    var messagingCredentials = check credentials.credentialsForRESTMessagingApi();
    log:printDebug("Credentials: " + messagingCredentials.toJsonString());

    var tokenEndPoint = check new sem:TokenEndpointClient(messagingCredentials.oa2);
    var token = check tokenEndPoint.requestAccessToken();
    log:printDebug(string`Received access token: ${string:substring(token.access_token, 0, 20)}...`);    
    var messagingClient = check new sem:MessagingApiClient(messagingCredentials.uri, accessToken = token.access_token);
    var response = messagingClient->receiveFromQueue(queueName = queueName, qos = qos);
    if response is error {
        return response;    
    }
    if response == () {
        log:printInfo("No message received. Queue is currently empty.");
        return;
    }
    log:printInfo("Received a message: ", messageId = response.id, contentType = response.contentType, payload = response.payload);
}

public function main(string action, *Options options) returns error? {
    match action {
        "receive" => {
            var result = receiveMessage(options);
            if result is error {
                log:KeyValues details = check 'error:detail(result).ensureType();
                log:printError("Error occured", 'error = result, stackTrace = 'error:stackTrace(result), keyValues = details);
            }
        }
    }
}