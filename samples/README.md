# Configuring credentials for the samples

```bash
# login to SAP Cloud Foundy API
$ cf cf login \
    -u <your SAP ID email> \
    -p <you SAP ID password> \
    -a https://api.cf.us10.hana.ondemand.com

# get the credentials to access the SAP Event Mesh instance with the name
# <your-event-mesh-name>
$ cf service-key \
    <your-event-mesh-name> \
    <your-service-key-name> 
```

The output includes a JSON map with credentials to access SAP Event Mesh APIs. Copy the
output to a file `.event-mesh-credentials.json`.
```json
{
 "management": [
  {
   "oa2": {
    "clientid": "sb-clone-xbem-service-broker-5fcac8f74a234204b041d925d266c9dd-clone!b65422|xbem-service-broker-!b732",
    "clientsecret": "d36a9fc1-c8f8-4c39-84e6-8158bf050378$PcSS_1hONcCAhoQ4_fVB7Nx65b9nMTe39XhaMZNZRXY=",
    "granttype": "client_credentials",
    "tokenendpoint": "https://c6f79120trial.authentication.us10.hana.ondemand.com/oauth/token"
   },
   "uri": "https://enterprise-messaging-hub-backend.cfapps.us10.hana.ondemand.com"
  }
 ],
  "broker": {
    "type": "sapmgw"
   },
   "oa2": {
    "clientid": "sb-clone-xbem-service-broker-5fcac8f74a234204b041d925d266c9dd-clone!b65422|xbem-service-broker-!b732",
    "clientsecret": "d36a9fc1-c8f8-4c39-84e6-8158bf050378$PcSS_1hONcCAhoQ4_fVB7Nx65b9nMTe39XhaMZNZRXY=",
    "granttype": "client_credentials",
    "tokenendpoint": "https://c6f79120trial.authentication.us10.hana.ondemand.com/oauth/token"
   },
   "protocol": [
    "amqp10ws"
   ],
   "uri": "wss://enterprise-messaging-messaging-gateway.cfapps.us10.hana.ondemand.com/protocols/amqp10ws"
  },
  // ... and more
}
```

# Sample:  `manage_queues`

`manage_queues` is a ballerina command line tool to create, update, delete, and get
SAP Event Mesh queues.

## Run the sample 

```bash
# get all queues
$ bal run manage_queues -- \
    get-queues

# create a queue
$ bal run manage_queues -- \
    create-queue --queueName=foo/bar

# get a queue
$ bal run manage_queues -- \
    get-queue --queueName=foo/bar
 
# delete a queue
$ bal run manage_queues -- \
    delete-queue --queueName=foo/bar

# enabled a log level, i.e. log level DEBUG, for example
$ bal run manage_queues -- \
    get-queues \
    -Cballerina.log.level=DEBUG
```

# Sample `publish_to_queue`

`publish_to_queue` is a ballerina command line tool to publish a message to a 
SAP Event Mesh queue using the REST messaging API.

## Run the sample 

```bash
# publish a text message
$ bal run publish_to_queue -- publish \
    --queueName foo/bar \
    --message "Hello World!"

# publish a JSON message with additional publishing options (qos and messageExpiration)
$ bal run publish_to_queue -- publish \
    --queueName foo/bar \
    --message '{ "message" : "Hello World!" }' \
    --contentType "application/json" \
    --qos 1 \
    --messageExpiration 1000

# enabled a log level, i.e. log level DEBUG, for example
$ bal run publish_to_queue -- publish \
    --queueName foo/bar \
    --message "Hello World!" \
    -Cballerina.log.level=DEBUG
```


# Sample `receive_from_queue`

`receive_from_queue` is a ballerina command line tool to receive a message from a 
SAP Event Mesh queue using the REST messaging API.

## Run the sample 

```bash
# receive a message
$ bal run receive_from_queue -- receive \
    --queueName foo/bar

# enable qos=1. In this case retrieved messages have a unique 
# message ID.
$ bal run receive_from_queue -- receive \
    --queueName foo/bar \
    --qos 1

# enabled a log level, i.e. log level DEBUG, for example
$ bal run receive_from_queue -- receive \
    --queueName foo/bar \
    -Cballerina.log.level=DEBUG
```