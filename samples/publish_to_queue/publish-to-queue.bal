import ballerina/log;
import gubaer/sap_event_mesh as sem;
import ballerina/lang.'error;

const string DEFAULT_CREDENTIALS_FILE_PATH = ".event-mesh-credentials.json";
configurable string credentialsFilePath = DEFAULT_CREDENTIALS_FILE_PATH;

public type Options record {|
    string? queueName = ();
    string? message = ();
    string? contentType = ();
    int? qos = ();
    int? messageExpiration = ();
|};


public function buildPublishOptions(Options options) returns sem:PublishOptions | error {
    var builder = new sem:PublishOptionsBuilder();
    if options.contentType != () {
        builder = builder.withContentType(<string>options.contentType);
    }
    if options.qos != () {
        sem:QosType qos = check (<int>options.qos).ensureType();
        builder = builder.withQos(qos);
    }
    return builder.build();
}

public function publishMessage(Options options) returns error? {
    if options.queueName is () {
        return error("Missing option --queueName. Aborting.");
    }

    if options.message is () {
        return error("Missing option --message. Aborting.");
    }

    string queueName = <string>options.queueName;
    sem:Credentials credentials = check sem:loadCredentials(credentialsFilePath);
    var messagingCredentials = check credentials.credentialsForRESTMessagingApi();
    log:printDebug("Credentials: " + messagingCredentials.toJsonString());

    var tokenEndPoint = check new sem:TokenEndpointClient(messagingCredentials.oa2);
    var token = check tokenEndPoint.requestAccessToken();
    log:printDebug(string`Received access token: ${string:substring(token.access_token, 0, 20)}...`);    
    var messagingClient = check new sem:MessagingApiClient(messagingCredentials.uri, accessToken = token.access_token);

    var publishOptions = check buildPublishOptions(options);
    check messagingClient->publishToQueue(queueName, payload = <string> options.message, options = publishOptions);
    
    log:printInfo(string`Successfully sent message to queue '${queueName}'`);
}


public function main(string action, *Options options) returns error? {
    match action {
        "publish" => {
            var result = publishMessage(options);
            if result is error {
                log:printError("Error occured", 'error = result, stackTrace = 'error:stackTrace(result));
            }
        }
    }
}