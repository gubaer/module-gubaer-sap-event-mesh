import ballerina/jballerina.java;
import ballerina/url;


isolated function serializeFormParams(map<string> formParams) returns string | error {
    string[] keyValuePairs = [];
    foreach string key in formParams.keys() {
        var encodedKey = url:encode(key, "UTF-8");
        if encodedKey is error {
            return error("Failed to URL encode string", 'string=key);
        }
        var encodedValue = url:encode(formParams.get(key), "UTF-8");
        if encodedValue is error {
            return error("Failed to URL encode string", 'string=formParams.get(key));
        }
        keyValuePairs.push(string`${encodedKey}=${encodedValue}`);
    }      
    return string:'join("&", ...keyValuePairs);
}

isolated function replaceAll(string value, string 'match, string replacement) returns string {
    var valueHandle = java:fromString(value);
    var matchHandle = java:fromString('match);
    var replacementHandle = java:fromString(replacement);

    var resultHandle = _javaReplaceAll(valueHandle, matchHandle, replacementHandle);
    var resultAsString = java:toString(resultHandle);
    if resultAsString is () {
        return "";
    } else {
        return resultAsString;
    }
}

isolated function removeWhiteSpace(string value) returns string {
    return replaceAll(value, "\\s+", "");
}

isolated function _javaReplaceAll(handle value, handle matchHandle, handle replacementHandle) returns handle = @java:Method {
    name: "replaceAll",
    'class: "java.lang.String"
} external;

