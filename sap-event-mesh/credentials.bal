import ballerina/io;
import ballerina/file;

# Describes credentials to request an OAuth2 access token from a
# OAuth2 token endpoint.
# 
# + clientid - the client id
# + clientsecret - the client secret
# + granttype - the grant type
# + tokenendpoint - the URI of the token endpoint
public type OAuth2Credentials record {| 
    @display {label: "Client ID"}
    string clientid;
    @display {label: "Client Secret"}
    string clientsecret;
    @display {label: "Grant Type"}
    string granttype;
    @display {label: "Token Endpoint"}
    string tokenendpoint;
|};

# Describes access credentials and parameters for the management API. The `oa2` parameters
# are used to request an access token from an OAuth2 token endpoint. The 
# received OAuth2 access token is then used to access the management API.
# 
# + oa2 - OAuth2 parameters
# + uri - the base URI of the management API
public type ManagementAPICredentials record {|    
    @display {label: "OAuth2 parameters"}
    OAuth2Credentials oa2;
    @display {label: "Management API base URI"}
    string uri;
|};

# Describes access credentials and parameters for messaging API. The `oa2` parameters
# are used to request an access token from an OAuth2 token endpoint. The 
# received OAuth2 access token is then used to access the messaging API.
# 
# + broker - the broker 
# + oa2 - OAuth2 parameters
# + protocol - the protocol, i.e. `[httprest]` for the REST based API
# + uri - the base URI of the API
public type MessagingAPICredentials record {|
    @display {label: "Broker"}
    record {|
        @display {label: "Broker Type"}
        string 'type;
    |} broker;
    @display {label: "OAuth2 parameters"}
    OAuth2Credentials oa2;
    @display {label: "Supported Protocols"}
    string[] protocol;
    @display {label: "Messaging API base URI"}
    string uri;
|};

# Loads the credentials from a file.
# 
# + credentialsFilePath - path to the credentials file 
# 
# + return - credentials read from the file; `file:Error` if the file can't be accessed; 
#       `io:Error` if the content of the file can't be read as JSON; `error` if the JSON
#       content isn't a `map<json>` object
public function loadCredentials(string credentialsFilePath) returns Credentials | error {
    var condition = ! check file:test(credentialsFilePath, file:EXISTS) 
        || ! check file:test(credentialsFilePath, file:READABLE) 
        || check file:test(credentialsFilePath, file:IS_DIR);

    if condition {
        return error file:Error("Failed to load credentials from file. Path doesn't exist, isn't readable, or is a directory.", 
        credentailsFilePath=credentialsFilePath);
    }

    var value = io:fileReadJson(credentialsFilePath);
    if value is error {
        return value;
    } else {
        map<json> 'map = check value.ensureType();
        return new Credentials('map);
    }
}

# Description
public class Credentials {

    # the SAP Event Mesh credentials
    public final json credentials;

    # Creates SAP Event Mesh credentials
    # 
    # + credentials - the credentials. Either a json object or a json string  
    public function init(map<json> | string credentials) returns error? {
        if credentials is string {
            self.credentials = check credentials.fromJsonString();
        } else {
            self.credentials = credentials;
        }
    }

    # Replies the parameters and credentials for accessing the management API of
    # the SAP Event Mesh service.
    # 
    # + return - Parameters and credentials for accessing the management API. Returns
    #   an `error` if there are no such credentials.
    # 
    public function credentialsForManagementAPI() returns ManagementAPICredentials | error {
        json[] entries = check self.credentials.management.ensureType();
        if entries.length() == 0 {
            return error("illegal credentials. list of 'management' credentials is empty.");
        }
        return check entries[0].fromJsonWithType(ManagementAPICredentials);
    }

    # Replies the parameters and credentials for accessing the REST based messaging API of
    # the SAP Event Mesh service.
    # 
    # + return - Parameters and credentials for accessing the REST based messaging API. Returns
    #   an `error` if there are no such credentials.
    # 
    public function credentialsForRESTMessagingApi() returns MessagingAPICredentials | error {       
        json[] entries = check self.credentials.messaging.ensureType();
        foreach json entry in entries {
            json[] protocols = check entry.protocol.ensureType();
            foreach json protocol in protocols {
                if protocol is string && protocol == "httprest" {
                    return check entry.fromJsonWithType(MessagingAPICredentials);
                }
            }
        }
        return error("credentials for messaging using REST API not found");
    }
}