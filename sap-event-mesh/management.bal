import ballerina/http;
import ballerina/url;
import ballerina/log;

public type QueueConfig record {|
    ("EXCLUSIVE"|"NON_EXCLUSIVE")? accessType;
    string? deadMsgQueue;
    int? maxDeliveredUnackedMsgsPerFlow;
    int? maxMessageSizeInBytes;
    int? maxQueueMessageCount;
    int? maxQueueSizeInBytes;
    int? maxDeliveryCount;
    int? maxTtl;
    boolean? respectTtl;
|};

public type QueueAccessType "EXCLUSIVE"|"NON_EXCLUSIVE";

public class QueueConfigBuilder {
    private QueueAccessType? accessType = ();
    private string? deadMsgQueue = ();
    private int? maxDeliveredUnackedMsgsPerFlow = ();
    private int? maxMessageSizeInBytes = ();
    private int? maxQueueMessageCount = ();
    private int? maxQueueSizeInBytes = ();
    private int? maxDeliveryCount = ();
    private int? maxTtl = ();
    private boolean? respectTtl = ();

    public function withAccessType(QueueAccessType accessType) returns QueueConfigBuilder {
        self.accessType = accessType;
        return self;
    }

    public function withDeadMsgQueue(string value) returns QueueConfigBuilder {
        self.deadMsgQueue = value;
        return self;
    }

    public function withMaxDeliveredUnackedMsgsPerFlow(int value) returns QueueConfigBuilder {
        self.maxDeliveredUnackedMsgsPerFlow = value;
        return self;
    }

    public function withMaxMessageSizeInBytes(int value) returns QueueConfigBuilder {
        self.maxMessageSizeInBytes = value;
        return self;
    }

    public function withMaxQueueMessageCount(int value) returns QueueConfigBuilder {
        self.maxQueueMessageCount = value;
        return self;
    }

    public function withMaxQueueSizeInBytes(int value) returns QueueConfigBuilder {
        self.maxQueueSizeInBytes = value;
        return self;
    }

    public function withMaxDeliveryCount(int value) returns QueueConfigBuilder {
        self.maxDeliveryCount = value;
        return self;
    }

    public function withMaxTtl(int value) returns QueueConfigBuilder {
        self.maxTtl = value;
        return self;
    }

    public function withRespectTtl(boolean value) returns QueueConfigBuilder {
        self.respectTtl = value;
        return self;
    }

    public function build() returns json|error {
        map<json> result = {};

        if self.accessType !is () {
            result["accessType"] = self.accessType.toString();
        }

        if self.deadMsgQueue !is () {
            result["deadMsgQueue"] = self.deadMsgQueue;
        }

        if self.maxDeliveredUnackedMsgsPerFlow !is () {
            if <int>self.maxDeliveredUnackedMsgsPerFlow <= 0 {
                return error("illegal value for 'maxDeliveredUnackedMsgsPerFlow'", required = "> 0", got = <int>self.maxDeliveredUnackedMsgsPerFlow);
            }
            result["maxDeliveredUnackedMsgsPerFlow"] = <int>self.maxDeliveredUnackedMsgsPerFlow;
        }

        if self.maxDeliveryCount !is () {
            if <int>self.maxDeliveryCount < 0 || <int>self.maxDeliveryCount > 255 {
                return error("illegal value for 'maxDeliveryCount'", required = "0 <= maxDeliveryCount <= 255", got = <int>self.maxDeliveryCount);
            }
            result["maxDeliveryCount"] = <int>self.maxDeliveryCount;
        }

        if self.maxMessageSizeInBytes !is () {
            if <int>self.maxMessageSizeInBytes <= 0 {
                return error("illegal value for 'maxMessageSizeInBytes'", required = "> 0", got = <int>self.maxMessageSizeInBytes);
            }
            result["maxMessageSizeInBytes"] = <int>self.maxMessageSizeInBytes;
        }

        if self.maxQueueMessageCount !is () {
            if <int>self.maxQueueMessageCount <= 0 {
                return error("illegal value for 'maxQueueMessageCount'", required = "> 0", got = <int>self.maxQueueMessageCount);
            }
            result["maxQueueMessageCount"] = <int>self.maxQueueMessageCount;
        }

        if self.maxQueueSizeInBytes !is () {
            if <int>self.maxQueueSizeInBytes <= 0 {
                return error("illegal value for 'maxQueueSizeInBytes'", required = "> 0", got = <int>self.maxQueueSizeInBytes);
            }
            result["maxQueueSizeInBytes"] = <int>self.maxQueueSizeInBytes;
        }

        if self.maxTtl !is () {
            if <int>self.maxTtl <= 0 {
                return error("illegal value for 'maxTtl'", required = "> 0", got = <int>self.maxTtl);
            }
            result["maxTtl"] = <int>self.maxTtl;
        }

        if self.respectTtl !is () {
            result["respectTtl"] = <boolean>self.respectTtl;
        }

        return result;
    }
}

public type Queue record {
    # the queue name 
    string name;
    # A queue has an access type, which determines how messages are delivered when multiple 
    # consumer flows are bound to it. Queues can be assigned one of the following access types: 
    # EXCLUSIVE, NON_EXCLUSIVE
    QueueAccessType? accessType = ();
    # The name of the Dead Message Queue (DMQ) used by the queue
    string? deadMsgQueue = ();
    # Maximum number of unacknowledged messages allowed in the queue. 
    # The default value is: 10000
    int? maxDeliveredUnackedMsgsPerFlow = ();
    # Maximum message size allowed in the queue (in bytes). The default value is: 10000000.
    int? maxMessageSizeInBytes = ();
    # Maximum allowed messages in the queue.
    int? maxQueueMessageCount = ();
    # Maximum allowed queue size (in bytes).
    # The default value is: 1572864000 in (binary) which equates to 1500 megabytes.
    int? maxQueueSizeInBytes = ();
    # Maximum number of times the Queue will attempt redelivery of a message prior to it being discarded 
    # or moved to the Dead Message Queue (DMQ). 0 <= maxRedeliveryCount <= 255
    int? maxRedeliveryCount = ();
    # Maximum time in seconds a message can stay in the Queue when Respect TTL option enabled. 
    # A message expires when the lesser of the sender assigned time-to-live (TTL) in the message 
    # and the maxTtl configured for the Queue, is exceeded. The expired mssage will move to 
    # Dead Message Queue (DMQ). A value of 0 disables expiry.
    int? maxTtl = ();
    # Current number of messages residing in the queue.
    int? messageCount = ();
    # Total size of messages in the queue (in bytes).
    int? queueSizeInBytes = ();
    # Enable or disable the respecting of the "time to live" (TTL). If enabled, then messages contained 
    # in the Queue are checked for expiry. If expired, the message is removed from the Queue 
    # and discarded. The default value is false.
    boolean? respectTtl = ();
    # Total count of unacknowledged messages in the queue.
    int? unacknowledgedMessageCount = ();
};

public type QueueSubscription record {|
    string queueName;
    string topicPattern;
|};

public type ManagementApiError distinct error;

type QueueList Queue[];

public isolated client class ManagementAPIClient {

    private final http:Client 'client;
    private final string accessToken;

    # Creates a client for the management API.
    #
    # + api - either the base URI to the management API (without the path) or an instance of 
    # `http:Client` which is already configured with the base URI 
    # + accessToken - the OAuth2 access token
    #
    # + returns - `http:ClientError` if the `http:Client` can't be constructed
    public isolated function init(string|http:Client api, string accessToken) returns (http:ClientError|error)? {
        if api is string {
            self.'client = check new (api);
        } else {
            self.'client = api;
        }
        self.accessToken = accessToken;
    }

    # Create or update a queue.
    #
    # + queueName - the name of the queue, i.e. `my-namespace/my-queue-name`
    # + queueConfig - optional queue configuration. Use `QueueConfigBuilder` to build a queue configuration
    #
    # + return - information about the created or updated queue; `url:Error` if the queue name can't be 
    # URL encoded; `http:ClientError` if invoking the remote API fails; `ManagementApiError` if the 
    # API returns a status code >= 400
    remote isolated function createOrUpdateQueue(string queueName, json? queueConfig = ())
        returns Queue|http:ClientError|url:Error|ManagementApiError|error {

        json config = queueConfig is () ? {} : queueConfig;

        var headers = {
            "Authorization": string `Bearer ${self.accessToken}`,
            "Content-Type": "application/json"
        };

        var encodedQueueName = check url:encode(queueName, "UTF-8");
        var path = string `/hub/rest/api/v1/management/messaging/queues/${encodedQueueName}`;

        var body = config.toJsonString();

        var response = check self.'client->put(path, headers = headers, message = body, targetType = http:Response);

        match response.statusCode {
            http:STATUS_OK
            |http:STATUS_CREATED => {
                log:printDebug("JSON response: " + check response.getTextPayload());
                return (check response.getJsonPayload()).cloneWithType(Queue);
            }
            http:STATUS_BAD_REQUEST
            |http:STATUS_UNAUTHORIZED
            |http:STATUS_FORBIDDEN
            |http:STATUS_NOT_FOUND
            |http:STATUS_NOT_IMPLEMENTED => {
                return error ManagementApiError("Failed to create or update queue", queueName = queueName,
                statusCode = response.statusCode, errorMessage = response.getTextPayload());
            }
            _ => {
                return error ManagementApiError("Failed to create or update queue. Unexpected status code.",
                    queueName = queueName, statusCode = response.statusCode,
                    errorMessage = response.getTextPayload());
            }
        }
    }

    # Get information about a queue.
    #
    # + queueName - the name of the queue, i.e. `my-namespace/my-queue-name`
    #
    # + return - information about the queue; `url:Error` if the queue name can't be 
    # URL encoded; `http:ClientError` if invoking the remote API fails; `ManagementApiError` if the 
    # API returns a status code >= 400
    remote isolated function getQueue(string queueName) returns Queue|http:ClientError|url:Error|ManagementApiError|error {
        var headers = {
            "Authorization": string `Bearer ${self.accessToken}`
        };

        var encodedQueueName = check url:encode(queueName, "UTF-8");
        var path = string `/hub/rest/api/v1/management/messaging/queues/${encodedQueueName}`;

        var response = check self.'client->get(path, headers = headers, targetType = http:Response);

        match response.statusCode {
            http:STATUS_OK => {
                return (check response.getJsonPayload()).cloneWithType(Queue);
            }
            http:STATUS_BAD_REQUEST
            |http:STATUS_UNAUTHORIZED
            |http:STATUS_FORBIDDEN
            |http:STATUS_NOT_FOUND => {
                return error ManagementApiError("Failed to get queue", queueName = queueName, statusCode = response.statusCode);
            }
            _ => {
                return error ManagementApiError("Failed to get queue queue. Unexpected status code.",
                    queueName = queueName, statusCode = response.statusCode);
            }
        }
    }

    # Get information about all queues.
    #
    # + return - information about all queues; `http:ClientError` if invoking the remote API fails; 
    # `ManagementApiError` if the API returns a status code >= 400
    remote isolated function getQueues() returns Queue[]|http:ClientError|url:Error|ManagementApiError|error {
        var headers = {
            "Authorization": string `Bearer ${self.accessToken}`
        };

        var path = "/hub/rest/api/v1/management/messaging/queues";

        var response = check self.'client->get(path, headers = headers, targetType = http:Response);

        match response.statusCode {
            http:STATUS_OK => {
                return (check response.getJsonPayload()).cloneWithType(QueueList);
            }
            http:STATUS_UNAUTHORIZED
            |http:STATUS_FORBIDDEN
            |http:STATUS_NOT_FOUND => {
                return error ManagementApiError("Failed to get queues", statusCode = response.statusCode);
            }
            _ => {
                return error ManagementApiError("Failed to get queues. Unexpected status code.",
                    statusCode = response.statusCode);
            }
        }
    }

    # Delete a queue.
    #
    # + queueName - the name of the queue, i.e. `my-namespace/my-queue-name`
    #
    # + return - information about the queue; `url:Error` if the queue name can't be 
    # URL encoded; `http:ClientError` if invoking the remote API fails; `ManagementApiError` if the 
    # API returns a status code >= 400
    remote isolated function deleteQueue(string queueName) returns (http:ClientError|url:Error|ManagementApiError|error)? {
        var headers = {
            "Authorization": string `Bearer ${self.accessToken}`
        };

        var encodedQueueName = check url:encode(queueName, "UTF-8");
        var path = string `/hub/rest/api/v1/management/messaging/queues/${encodedQueueName}`;

        var response = check self.'client->delete(path, headers = headers, targetType = http:Response);

        match response.statusCode {
            http:STATUS_OK|
            http:STATUS_NO_CONTENT => {
                return;
            }
            http:STATUS_BAD_REQUEST|
            http:STATUS_UNAUTHORIZED|
            http:STATUS_FORBIDDEN|
            http:STATUS_NOT_FOUND => {
                return error ManagementApiError("Failed to delete queue", queueName = queueName, statusCode = response.statusCode);
            }
            _ => {
                return error ManagementApiError("Failed to delete queue. Unexpected status code.",
                    queueName = queueName, statusCode = response.statusCode);
            }
        }
    }
}

