import ballerina/http;
import ballerina/url;
import ballerina/log;

public const string HEADER_MESSAGE_EXPIRATION = "X-Message-Expiration";
public const string HEADER_QOS = "X-Qos";
public const string HEADER_CONTENT_TYPE = "Content-Type";
public const string HEADER_MESSAGE_ID = "X-Message-Id";

# The allowed values for specifying QoS are 0 and 1. The API for publishing messages 
# requires a mandatory header x-qos. When the client calls the API for publishing messages 
# with QoS 0, the service tries to deliver the message and returns an HTTP response with 
# code 204 irrespective of whether the message is delivered. The best effort is made 
# to deliver the message but, doesn’t guarantee that the message is sent to queue or topic. 
# If a message client calls the API for publishing messages with QoS 1 for guaranteed delivery, 
# then, the service responds with the HTTP response code 204. If the 204 response code 
# isn’t received, it’s the client’s responsibility to retry until the response code 204 is 
# received.
# Source: [SAP Event Mesh][https://help.sap.com/viewer/bf82e6b26456494cbdd197057c09979f/Cloud/en-US/6a0e4c77e3014acb8738af039bd9df71.html]
public type QosType 0|1;

# Default QoS is 0
public const QosType DEFAULT_QOS = 0;

# Optional parameters when publishing a message
public type PublishOptions record {|
    # The content type. If missing a suitable content type is derived from the
    # type of payload, i.e. `application/json` for JSON payload.
    string? contentType = ();

    # the quality of service. See `QosType`.
    QosType qos = DEFAULT_QOS;

    # With every publish request, the message client can set a time to live for the 
    # message in milliseconds. If the API is called without this header, the default 
    # value of 2592000000 (30 days) is used.
    # Source: [SAP Event Mesh][https://help.sap.com/viewer/bf82e6b26456494cbdd197057c09979f/Cloud/en-US/6a0e4c77e3014acb8738af039bd9df71.html]
    int? messageExpiration = ();
|};

# Builder for `PublishOptions`.
public class PublishOptionsBuilder {
    private string? contentType = ();
    private QosType qos = DEFAULT_QOS;
    private int? messageExpiration = ();

    # Sets the content type.
    #
    # + contentType - the content type
    # + return - the builder
    public isolated function withContentType(string contentType) returns PublishOptionsBuilder {
        self.contentType = contentType;
        return self;
    }

    # Sets the quality of service.
    #
    # + qos - the quality of service
    # + return - the builder
    public isolated function withQos(QosType qos) returns PublishOptionsBuilder {
        self.qos = qos;
        return self;
    }

    # Sets the message expiration.
    #
    # + value - the message expiration time. 0 <= `value` <= 2592000000 required.
    # + return - the builder or an error if `value` is out of range
    public isolated function withMessageExpiration(int value) returns PublishOptionsBuilder {
        self.messageExpiration = value;
        return self;
    }

    # Configures the builder with publish options.
    #
    # + options - the publish options
    # + return - the builder
    public isolated function withPublishOptions(PublishOptions? options) returns PublishOptionsBuilder {
        if options is () {
            return self;
        }
        self.contentType = options.contentType;
        self.qos = options.qos;
        self.messageExpiration = options.messageExpiration;
        return self;
    }

    # Builds the publish options.
    #
    # + return - the `PublishOptions`
    public isolated function build() returns PublishOptions|error {
        if self.messageExpiration !is () {
            if <int>self.messageExpiration < 1 || <int>self.messageExpiration > 2592000000 {
                return error("message expiration out of range", expected = "1 <= value <= 2592000000", value = self.messageExpiration);
            }
        }
        return {
            contentType: self.contentType,
            qos: self.qos,
            messageExpiration: self.messageExpiration
        };
    }
}

# Replied by methods of `MessagingApiClient`
public type MessagingApiClientError distinct error;

# A message retrieved from a queue
public type EnqueuedMessage record {|
    # the unique id of the message. Only available if the message is retrieved
    # with qos=1, otherwise the empty string.
    string id;
    # the content type of the message
    string contentType;
    # the payload of the message
    string|json|byte[] payload;
|};

# A client for the REST-based messaging API for the SAP Event Mesh.
public client class MessagingApiClient {
    final http:Client 'client;
    private final string accessToken;

    # Creates the client.
    #
    # + api - either the base URI of the API or a `http:Client` configured with the 
    # base URI. 
    # + accessToken - the OAuth2 access token
    #
    # + returns - `http:Error` if the `http:Client` can't be build
    public function init(string|http:Client api, string accessToken) returns http:Error? {
        if api is string {
            self.'client = check new (api);
        } else {
            self.'client = api;
        }
        self.accessToken = accessToken;
    }

    # Publishes a message to a SAP Event Mesh queue.
    #
    # + queueName - the name of the queue
    # + payload - the message payload
    # + options - publish options like the required quality of service (QoS) or the message 
    # expiration time
    # + return - an error object: `MessagingApiClientError` if the API call fails; `http:ClientError` if the
    # remote API can't be invoked (timeout, DNS problems, etc.); `url:Error` if the `queueName` can't be
    # URL encoded; `error` if publish options have invalid values
    remote isolated function publishToQueue(string queueName, string|map<json>|json[]|byte[] payload, PublishOptions? options = ())
        returns (MessagingApiClientError|url:Error|http:ClientError|error)? {

        var encodedQueueName = check url:encode(queueName, "UTF-8");
        var path = string `/messagingrest/v1/queues/${encodedQueueName}/messages`;

        var publishOptions = check new PublishOptionsBuilder().withPublishOptions(options).build();

        map<string> headers = {
            "Authorization": string `Bearer ${self.accessToken}`
        };

        if publishOptions.contentType !is () {
            headers[HEADER_CONTENT_TYPE] = <string>publishOptions.contentType;
        } else {
            match payload {
                var j if j is map<json> || j is json[] => {
                    headers[HEADER_CONTENT_TYPE] = "application/json";
                }
                var s if s is string => {
                    headers[HEADER_CONTENT_TYPE] = "text/plain";
                }
                var b if b is byte[] => {
                    headers[HEADER_CONTENT_TYPE] = "application/octet-stream";
                }
            }
        }
        headers[HEADER_QOS] = publishOptions.qos.toString();
        if publishOptions.messageExpiration !is () {
            headers[HEADER_MESSAGE_EXPIRATION] = publishOptions.messageExpiration.toString();
        }

        // TODO(guaber): in case of publishOptions.qos == 1 we should retry to publish
        // in a loop until status code 204 is received.
        var response = check self.'client->post(path, headers = headers, message = payload, targetType = http:Response);

        match response.statusCode {
            http:STATUS_NO_CONTENT => {
                return;
            }
            http:STATUS_BAD_REQUEST => {
                var message = self.getErrorMessage(response);
                if message != () {
                    return error MessagingApiClientError("Failed to publish message.",
                        statusCode = response.statusCode, errorMessage = message);
                } else {
                    return error MessagingApiClientError("Failed to publish message.",
                        statusCode = response.statusCode);
                }
            }
            http:STATUS_NOT_FOUND => {
                return error MessagingApiClientError("Queue not found.", statusCode = response.statusCode,
                    queueName = queueName);
            }
            _ => {
                return error MessagingApiClientError("Failed to publish message. Unexpected error code.",
                    statusCode = response.statusCode);
            }
        }
    }

    # Receives a message from a queue if at least one message is available.
    #
    # Returns a payload of type `string` if the content type is `text/plain`.
    # Returns a payload of type `json` if the content type is `application/json` and the 
    # payload can be parsed as JSON object.
    # Otherwise returns a payload of type `byte[]`.
    #
    # + queueName - the name of the queue
    # + qos - quality of service
    #
    # + return - the retrieved message or `()` if no message is availble. `MessagingApiClientError` if the API
    # responds with an error code. `http:Error` if communication with the API fails (DNS, timeout, etc.). 
    # `url:Error` if URL encoding the queue name fails.
    isolated remote function receiveFromQueue(string queueName, QosType qos = DEFAULT_QOS)
        returns EnqueuedMessage?|MessagingApiClientError|http:Error|url:Error {

        map<string> headers = {
            "Authorization": string `Bearer ${self.accessToken}`
        };
        headers[HEADER_QOS] = qos.toString();
        log:printDebug(string `headers: ${headers.toJsonString()}`);

        var encodedQueueName = check url:encode(queueName, "UTF-8");
        var path = string `/messagingrest/v1/queues/${encodedQueueName}/messages/consumption`;

        var response = check self.'client->post(path = path, message = "", headers = headers, targetType = http:Response);

        match response.statusCode {
            http:STATUS_OK => {
                log:printDebug("Names of received headers:", headerNames = response.getHeaderNames());
                var header = response.getHeader(HEADER_MESSAGE_ID);
                string id;
                if header is error {
                    log:printWarn(string `Received a message without a '${HEADER_MESSAGE_ID}' header`);
                    id = "";
                } else {
                    id = <string>header;
                }

                string contentType = response.getContentType();

                json|string|byte[] payload;
                match contentType {
                    "text/plain" => {
                        payload = check response.getTextPayload();
                    }

                    "application/json" => {
                        var body = response.getJsonPayload();
                        if body is json {
                            payload = body;
                        } else {
                            payload = check response.getTextPayload();
                            log:printWarn("Failed to parse JSON payload. Using payload as text.", payload = payload);
                        }
                    }

                    _ => {
                        payload = check response.getBinaryPayload();
                    }
                }

                return {
                    id: id,
                    contentType: contentType,
                    payload: payload
                };
            }
            http:STATUS_NO_CONTENT => {
                return ();
            }
            http:STATUS_NOT_FOUND => {
                return error MessagingApiClientError("Queue not found.", statusCode = response.statusCode,
                    queueName = queueName);
            }
            _ => {
                var errorPayload = response.getTextPayload();
                if errorPayload is error {
                    return error MessagingApiClientError("Failed to consume message. Unexpected error code.",
                        statusCode = response.statusCode);
                } else {
                    return error MessagingApiClientError("Failed to consume message. Unexpected error code.",
                        statusCode = response.statusCode, errorMessage = errorPayload);
                }
            }
        }
    }

    private isolated function getErrorMessage(http:Response response) returns string? {
        var body = response.getJsonPayload();
        if body is map<json> {
            var message = body.message;
            if message !is error {
                var value = message.ensureType(string);
                if value !is error {
                    return value;
                }
            }
        }
        return ();
    }
}
