import ballerina/http;
import ballerina/mime;

# An access token replied by an OAuth token endpoint
# 
# + access_token - the access token. Can be used in the `Authorization` header of 
#       a http request
# + token_type - the type of the token 
# + expires_in - when the token expires. Number of seconds relative to the time the
#       token was requested
# + scope - the scope
# + jti - unique identifier for the token
public type AccessToken record {|
    string access_token;
    string token_type;
    int expires_in;
    string scope;
    string jti;
|};


public type TokenEndpointClientError distinct error;

# A client to request an OAuth2 access token to access SAP Event Mesh 
# APIs.
# 
public client class TokenEndpointClient {
    # the credentials used to request access tokens
    public final OAuth2Credentials & readonly credentials;

    private final http:Client 'client;

    # Creates the client
    # 
    # + credentials -  the credentials used to request access tokens
    # + client - a `http:Client` which accesses the remote token endpoint. Optional. If missing
    #       a new `http:Client` is created. Can be used to inject a mock `http:Client` in unit
    #       tests.
    public function init(OAuth2Credentials credentials, http:Client? 'client=()) returns error? {
        self.credentials = credentials.cloneReadOnly();
        if 'client is () {
            self.'client = check new http:Client(self.credentials.tokenendpoint);
        } else {
            self.'client = 'client;
        }
    }

    # Requests an OAuth2 access token.
    # 
    # + return - the OAuth2 access token. `http:ClientError` if communication with the remote API fails. 
    #       `TokenEndpointClientError` if the token endpoint replies an error status code.
    public isolated function requestAccessToken() returns AccessToken | http:ClientError | error {
        string auth_header = <string> check mime:base64Encode(string`${self.credentials.clientid}:${self.credentials.clientsecret}`);
        auth_header = removeWhiteSpace(auth_header);
            
        auth_header = string`Basic ${auth_header}`;
        var headers = {
            "Authorization": auth_header,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var formParams = {
            "grant_type" : "client_credentials",
            "response_type" : "token"
        };
        var payload = check serializeFormParams(formParams);
        var response = check self.'client->post("", headers = headers, message = payload, targetType = http:Response);

        if response.statusCode == http:STATUS_OK {
            return (check response.getJsonPayload()).cloneWithType(AccessToken);
        } else {
            return error TokenEndpointClientError("Failed to request access token.", 
                statusCode=response.statusCode, 
                errorMessage=check response.getTextPayload());
        }        
    }
}