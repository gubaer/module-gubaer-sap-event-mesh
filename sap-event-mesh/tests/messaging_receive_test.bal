import ballerina/test;
import ballerina/http;
import ballerina/lang.'error;
import ballerina/io;

@test:Config
function testReceiveFromQueue_OKCase_text() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        string payload = "hello world!";
        response.setPayload(payload);
        response.setHeader(HEADER_MESSAGE_ID, "msg-01");
        response.setHeader(HEADER_CONTENT_TYPE, "text/plain");
        response.statusCode = http:STATUS_OK;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());

    var 'client = check new MessagingApiClient(api = mockClient, accessToken = "an-access-token");
    var result = 'client->receiveFromQueue(queueName);
    test:assertFalse(result is error);
    var message = check result.ensureType(EnqueuedMessage);
    test:assertEquals(message.id, "msg-01");
    test:assertEquals(message.contentType, "text/plain");
    test:assertTrue(message.payload is string);
    string payload = check message.payload.ensureType();
    test:assertEquals(payload, "hello world!");
}

@test:Config
function testReceiveFromQueue_OKCase_mapjson() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        map<json> payload = {
            "message": "hello world!"
        };
        response.setPayload(payload);
        response.setHeader(HEADER_MESSAGE_ID, "msg-01");
        response.setHeader(HEADER_CONTENT_TYPE, "application/json");
        response.statusCode = http:STATUS_OK;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());

    var 'client = check new MessagingApiClient(api = mockClient, accessToken = "an-access-token");
    var result = 'client->receiveFromQueue(queueName);
    test:assertFalse(result is error);
    var message = check result.ensureType(EnqueuedMessage);
    test:assertEquals(message.id, "msg-01");
    test:assertEquals(message.contentType, "application/json");
    test:assertTrue(message.payload is map<json>);
    map<json> payload = check message.payload.ensureType();
    test:assertEquals(payload.message, "hello world!");
}

@test:Config
function testReceiveFromQueue_OKCase_listjson() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        json[] payload = ["hello world!"];
        response.setPayload(payload);
        response.setHeader(HEADER_MESSAGE_ID, "msg-01");
        response.setHeader(HEADER_CONTENT_TYPE, "application/json");
        response.statusCode = http:STATUS_OK;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());

    var 'client = check new MessagingApiClient(api = mockClient, accessToken = "an-access-token");
    var result = 'client->receiveFromQueue(queueName);
    test:assertFalse(result is error);
    var message = check result.ensureType(EnqueuedMessage);
    test:assertEquals(message.id, "msg-01");
    test:assertEquals(message.contentType, "application/json");
    test:assertTrue(message.payload is json[]);
    json[] payload = check message.payload.ensureType();
    test:assertEquals(check payload[0].ensureType(string), "hello world!");
}

@test:Config
function testReceiveFromQueue_OKCase_bytearray() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        byte[] payload = [0, 1, 2, 3, 4, 5, 6];
        response.setPayload(payload);
        response.setHeader(HEADER_MESSAGE_ID, "msg-01");
        response.setHeader(HEADER_CONTENT_TYPE, "application/octet-stream");
        response.statusCode = http:STATUS_OK;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());

    var 'client = check new MessagingApiClient(api = mockClient, accessToken = "an-access-token");
    var result = 'client->receiveFromQueue(queueName);
    test:assertFalse(result is error);
    var message = check result.ensureType(EnqueuedMessage);
    test:assertEquals(message.id, "msg-01");
    test:assertEquals(message.contentType, "application/octet-stream");
    test:assertTrue(message.payload is byte[]);
    byte[] payload = check message.payload.ensureType();
    test:assertEquals(payload, [0, 1, 2, 3, 4, 5, 6]);
}

@test:Config
function testReceiveFromQueue_NoMessageAvailable() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        response.statusCode = http:STATUS_NO_CONTENT;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());

    var 'client = check new MessagingApiClient(api = mockClient, accessToken = "an-access-token");
    var result = 'client->receiveFromQueue(queueName);
    test:assertFalse(result is error);
    test:assertEquals(result, (), "Expected null as response because queue is empty");
}

@test:Config
function testReceiveFromQueue_QueueNotFound() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        response.statusCode = http:STATUS_NOT_FOUND;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());

    var 'client = check new MessagingApiClient(api = mockClient, accessToken = "an-access-token");
    var result = 'client->receiveFromQueue(queueName);
    test:assertTrue(result is MessagingApiClientError);
    if result is error {
        var details = 'error:detail(result);
        io:println(details["statusCode"], 404);
        io:println(details["queueName"], "test/test");
    }
}
