import ballerina/test;
import ballerina/io;
import ballerina/http;


@test:Config
function testRequestAccessToken() returns error? {
    var credentials = {
        "clientid": "sb-clone-xbem-service-broker-5fcac8f74a234204b041d925d266c9dd-clone!b65422|xbem-service-broker-!b732",
        "clientsecret": "d36a9fc1-c8f8-4c39-84e6-8158bf050378$PcSS_1hONcCAhoQ4_fVB7Nx65b9nMTe39XhaMZNZRXY=",
        "granttype": "client_credentials",
        "tokenendpoint": "https://c6f79120trial.authentication.us10.hana.ondemand.com/oauth/token"
    };
    var 'client = check new TokenEndpointClient(credentials);
    var token = check 'client.requestAccessToken();
    io:print(token);
}

@test:Config
function testRequestAccessToken_OKCase() returns error? {
    var credentials = {
        "clientid": "client-id",
        "clientsecret": "client-secret",
        "granttype": "client_credentials",
        "tokenendpoint": "https://token-endpoint/oauth/token"
    };

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        json payload = {
            "error" : "invalid_request",
            "error_description" : "an error description"
        };
        response.setPayload(payload);
        response.statusCode = http:STATUS_BAD_REQUEST;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("post").thenReturn(buildResponse());
    var 'client = check new TokenEndpointClient(credentials, 'client = mockClient);
    var token = 'client.requestAccessToken();
    if token is error {
        test:assertTrue(true);
    } else {
        test:assertTrue(false, "Should have returned an error");
    }

}