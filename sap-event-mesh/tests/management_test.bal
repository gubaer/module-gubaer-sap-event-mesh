import ballerina/test;
import ballerina/http;
import ballerina/io;

@test:Config
function testCreateOrUpdateQueue_OKCase() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        json payload = {
            "name" : queueName
        };
        response.setPayload(payload);
        response.statusCode = http:STATUS_OK;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("put").thenReturn(buildResponse());
    var 'client = check new ManagementAPIClient(api = mockClient, accessToken = "an-access-token");
    var queue = 'client->createOrUpdateQueue(queueName);
    if queue is error {
        test:assertFail("Didn't expect an error: " + queue.toString());
    } else {
        test:assertEquals(queue.name, queueName);
    }    
}

@test:Config
function testCreateOrUpdateQueue_ErrorCase() returns error? {
    var queueName = "test/test";

    var buildResponse = function() returns http:Response {
        http:Response response = new;
        response.statusCode = http:STATUS_BAD_REQUEST;
        return response;
    };

    var mockClient = test:mock(http:Client);
    test:prepare(mockClient).when("put").thenReturn(buildResponse());
    var 'client = check new ManagementAPIClient(api = mockClient, accessToken = "an-access-token");
    var queue = 'client->createOrUpdateQueue(queueName);
    if queue is error {
        io:println("Got an error: " + queue.toString());
        test:assertTrue(true);
    } else {
        test:assertFail("Expected an error");
    }    
}