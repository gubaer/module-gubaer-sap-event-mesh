import ballerina/test;
import ballerina/http;

public map<string|string[]>? receivedHeaders = ();
public http:RequestMessage receivedMessage = "";

public client class PublishToQueueMock {
    remote function post(string path, http:RequestMessage message, map<string|string[]>? headers, string? mediaType, http:TargetType targetType = http:Response) returns @tainted http:Response|http:PayloadType|http:ClientError {
        receivedHeaders = headers;
        receivedMessage = message;
        http:Response response = new;
        response.statusCode = http:STATUS_NO_CONTENT;
        return response;
    }
}

@test:Config
function testPublishToQueue_OKCase_TextPayload() returns error? {
    var queueName = "test/test";

    // unfortunately not possible at the moment. Leads to an exception.
    // 
    // var mock = client object {
    //      remote function post(string path, http:RequestMessage message, map<string|string[]>? headers, string? mediaType, http:TargetType targetType = http:Response) returns @tainted http:Response| http:PayloadType | http:ClientError {
    //          savedMessage = message;
    //          return new http:Response();
    //      }
    // };

    var mock = new PublishToQueueMock();

    var mockApi = <http:Client>test:mock(http:Client, mock);

    var 'client = check new MessagingApiClient(api = mockApi, accessToken = "an-access-token");
    var result = 'client->publishToQueue(queueName, "Hello World!");
    test:assertFalse(result is error);

    test:assertTrue(receivedHeaders !is ());
    if receivedHeaders !is () {
        test:assertEquals(receivedHeaders[HEADER_CONTENT_TYPE], "text/plain");
        test:assertEquals(receivedHeaders[HEADER_QOS], DEFAULT_QOS.toString());
        test:assertEquals(receivedHeaders[HEADER_MESSAGE_EXPIRATION], ());
    }
    test:assertTrue(receivedMessage is string);
    if receivedMessage is string {
        test:assertEquals(<string>receivedMessage, "Hello World!");
    }
}

@test:Config
function testPublishToQueue_OKCase_JsonMapPayload() returns error? {
    var queueName = "test/test";
    var mock = new PublishToQueueMock();
    var mockApi = <http:Client>test:mock(http:Client, mock);

    var 'client = check new MessagingApiClient(api = mockApi, accessToken = "an-access-token");
    map<json> payload = {
        "message": "Hello World!"
    };
    var result = 'client->publishToQueue(queueName, payload = payload);
    test:assertFalse(result is error);

    test:assertTrue(receivedHeaders !is ());
    if receivedHeaders !is () {
        test:assertEquals(receivedHeaders[HEADER_CONTENT_TYPE], "application/json");
        test:assertEquals(receivedHeaders[HEADER_QOS], DEFAULT_QOS.toString());
        test:assertEquals(receivedHeaders[HEADER_MESSAGE_EXPIRATION], ());
    }
    test:assertTrue(receivedMessage is map<json>);
    if receivedMessage is map<json> {
        map<json> 'map = check receivedMessage.ensureType();
        test:assertEquals('map.message, "Hello World!");
    }
}

@test:Config
function testPublishToQueue_OKCase_JsonListPayload() returns error? {
    var queueName = "test/test";
    var mock = new PublishToQueueMock();
    var mockApi = <http:Client>test:mock(http:Client, mock);

    var 'client = check new MessagingApiClient(api = mockApi, accessToken = "an-access-token");
    json[] payload = [
        "Hello World!"
    ];
    var result = 'client->publishToQueue(queueName, payload = payload);
    test:assertFalse(result is error);

    test:assertTrue(receivedHeaders !is ());
    if receivedHeaders !is () {
        test:assertEquals(receivedHeaders[HEADER_CONTENT_TYPE], "application/json");
        test:assertEquals(receivedHeaders[HEADER_QOS], DEFAULT_QOS.toString());
        test:assertEquals(receivedHeaders[HEADER_MESSAGE_EXPIRATION], ());
    }
    test:assertTrue(receivedMessage is json[]);
    if receivedMessage is json[] {
        json[] list = check receivedMessage.ensureType();
        test:assertEquals(list[0], "Hello World!");
    }
}

@test:Config
function testPublishToQueue_OKCase_WithCustomContentType() returns error? {
    var queueName = "test/test";
    var mock = new PublishToQueueMock();
    var mockApi = <http:Client>test:mock(http:Client, mock);

    var 'client = check new MessagingApiClient(api = mockApi, accessToken = "an-access-token");
    string payload = "Hello World!";
    PublishOptions options = check new PublishOptionsBuilder()
        .withContentType("application/my-text-type")
        .build();
    var result = 'client->publishToQueue(queueName, payload = payload, options = options);
    test:assertFalse(result is error);

    test:assertTrue(receivedHeaders !is ());
    if receivedHeaders !is () {
        test:assertEquals(receivedHeaders[HEADER_CONTENT_TYPE], "application/my-text-type");
        test:assertEquals(receivedHeaders[HEADER_QOS], DEFAULT_QOS.toString());
        test:assertEquals(receivedHeaders[HEADER_MESSAGE_EXPIRATION], ());
    }
    test:assertTrue(receivedMessage is string);
    if receivedMessage is string {
        string value = check receivedMessage.ensureType();
        test:assertEquals(value, "Hello World!");
    }
}

@test:Config
function testPublishToQueue_OKCase_WithOptions() returns error? {
    var queueName = "test/test";
    var mock = new PublishToQueueMock();
    var mockApi = <http:Client>test:mock(http:Client, mock);

    var 'client = check new MessagingApiClient(api = mockApi, accessToken = "an-access-token");
    string payload = "Hello World!";
    PublishOptions options = check new PublishOptionsBuilder()
        .withMessageExpiration(500)
        .withQos(1)
        .withContentType("application/my-text-type")
        .build();
    var result = 'client->publishToQueue(queueName, payload = payload, options = options);
    test:assertFalse(result is error);

    test:assertTrue(receivedHeaders !is ());
    if receivedHeaders !is () {
        test:assertEquals(receivedHeaders[HEADER_CONTENT_TYPE], "application/my-text-type");
        test:assertEquals(receivedHeaders[HEADER_QOS], 1.toString());
        test:assertEquals(receivedHeaders[HEADER_MESSAGE_EXPIRATION], 500.toString());
    }
    test:assertTrue(receivedMessage is string);
    if receivedMessage is string {
        string value = check receivedMessage.ensureType();
        test:assertEquals(value, "Hello World!");
    }
}
