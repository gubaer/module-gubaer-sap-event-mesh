import ballerina/test;


@test:Config
function testReplaceAll() returns error? {
    var value = "hello world";
    var replaced = replaceAll(value, "\\s+", "");
    test:assertEquals(replaced, "helloworld");
}
