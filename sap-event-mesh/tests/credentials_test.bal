//import ballerina/log;
//import ballerina/os;
import ballerina/test;
import ballerina/io;
//import ballerina/file;

@test:Config
function testCreateCredentialsFromString() returns error? {
    var 'source = string `{ "foo" : "bar"}`;
    var credentials = check new Credentials('source);
    io:println(credentials.credentials);
}

@test:Config
function testGetManagementCredentials_OK() returns error? {
    var jsonText = string `
        {
            "management": [{
                "oa2": {
                    "clientid": "sb-clone-xbem-service-broker-443fd3883db24d2d93249ccc38116836-clone!b65422|xbem-service-broker-!b732",
                    "clientsecret": "ad88d737-2cf6-4539-84e9-9f484edd249f$mfJxiXryE6a1ey2EnlQAzgrdk6nrBvSO8OWkik4tvao=",
                    "granttype": "client_credentials",
                    "tokenendpoint": "https://c6f79120trial.authentication.us10.hana.ondemand.com/oauth/token"
                },
                "uri": "https://enterprise-messaging-hub-backend.cfapps.us10.hana.ondemand.com"
            }]
        }`;
    var credentials = check new Credentials(jsonText);
    ManagementAPICredentials managementCredentials = check credentials.credentialsForManagementAPI();
    test:assertEquals(managementCredentials.oa2.clientid, "sb-clone-xbem-service-broker-443fd3883db24d2d93249ccc38116836-clone!b65422|xbem-service-broker-!b732");
}


@test:Config
function testGetRestBasedMessagingCredentials_OK() returns error? {
    var _ = string `{
        "management": [
            {
            "oa2": {
                "clientid": "sb-clone-xbem-service-broker-443fd3883db24d2d93249ccc38116836-clone!b65422|xbem-service-broker-!b732",
                "clientsecret": "ad88d737-2cf6-4539-84e9-9f484edd249f$mfJxiXryE6a1ey2EnlQAzgrdk6nrBvSO8OWkik4tvao=",
                "granttype": "client_credentials",
                "tokenendpoint": "https://c6f79120trial.authentication.us10.hana.ondemand.com/oauth/token"
            },
            "uri": "https://enterprise-messaging-hub-backend.cfapps.us10.hana.ondemand.com"
            }
        ]}`;
}
