Provides clients to access APIs for [SAP Event Mesh][sap-event-mesh]:

* client to access the [Management API][management-rest-api-doc]
* client to access the [REST-based Messaging API][messaging-rest-api-doc]

## Package Overview
TBD

## Compatibility

|                            | Version                              |
|----------------------------|--------------------------------------|
| Ballerina Language         | Ballerina 2201.0.1 (Swan Lake)       |

### Report issues
TBD

[sap-event-mesh]: https://help.sap.com/viewer/bf82e6b26456494cbdd197057c09979f/Cloud/en-US
[messaging-rest-api-doc]:https://help.sap.com/doc/3dfdf81b17b744ea921ce7ad464d1bd7/Cloud/en-US/messagingrest-api-spec.html
[management-rest-api-doc]:https://help.sap.com/doc/75c9efd00fc14183abc4c613490c53f4/Cloud/en-US/rest-management-messaging.html
